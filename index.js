// Use the "require" directive to load Node.js module
	// A "module" is a software component or part of a program that contains one or more routines.
// "http module" lets Node.js to transfer data using the Hypertext Transfer Protocol.
	// This is a set of individual files that contain code to create a "comment" that helps establish data transfer between application.
	// allows us to fetch resources such as HTML document.
//Client (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (requests/responses)
// The message sent by the client is callled "request"
// The message sent by the server as an answer is called "response".
let http = require("http");

// Using this module's "createServer()" method, we can create an HTTP server that listens to the request on a specified port and gives responses back to the client.

// A port is a virtual point where network connections start and end.
// Each port is associated with specific process or services.
// The server will be assigned to port 4000 via the "listen()" method where the server will listen to any request that are sent to it and will also send theh response on via this port.
http.createServer(function(request, response){
	// Use to the writeHead() method to:
		// Set status code for the response. (200 means "OK")
		// Set the content-type of the response. (plain text message)
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Hello World");

	/*response.writeHead(200, {"Content-Type": "text/html"});
	response.end("<h1>Hello World</h1>")*/;
}).listen(4000);
// npx kill-port 4000 - Kill a port
//  npm install -g nodemon // sudo npm install -g nodemon for mac
// NPM - Node Package Manager
// 3000, 4000, 8000, 5000 - Usually used for web development.
// We will access the server in the browser using the localhost:4000
console.log("Server is running at the localhost:4000");
